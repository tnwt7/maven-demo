package com.tnwt.maven.demo;

public class Device2 {
	private String name = "device";
	private double power = 100.00;
	private double duration = 1.00;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name == "" || name == null)
		{
			throw new RuntimeException();
		}
		this.name = name;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power)
	{
		if(power >10000.00 || power <= 0.00)
		{
			throw new RuntimeException();
		}
		this.power = power;
		
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		if(duration > 24.00 || duration <= 0.00)
		{
			throw new RuntimeException();
		}
		this.duration = duration;
	}
}
